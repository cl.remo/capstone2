const jwt = require('jsonwebtoken');
const secret = "ECommerceAPIB303";

// Generating a token
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// The jwt.sign() will generate a token using the user data and secret key. The 3rd argument serves as additional options for the token, like adding an expiry date/time.
	return jwt.sign(data, secret, {});
}

// verifying a token
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if (typeof token === "undefined") {
		return response.send({auth: 'Failed, please include token in the header of the request.'})
	} else {
		console.log(token);

		token = token.slice(7, token.length);

		console.log(token);	
	}

	jwt.verify(token, secret, (error, decodedToken) => {
		if (error) {
			return response.send({
				auth: 'Failed', 
				message: error.message
			})
		} else {
			console.log(decodedToken)
			request.user = decodedToken;
			next();
		}
	})
}

// verifying if user is admin
module.exports.verifyAdmin = (request, response, next) => {
	if (request.user.isAdmin) {
		return next();
	} else {
		return response.send({
			auth: 'Failed',
			message: 'Action Forbidden'
		})
	}
}