const User = require('../models/Users.js');
const Product = require('../models/Products.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		if (result.length <= 0){
			return false;
		}

		// This will only return true if there are no errors AND there is an existing user from the database.
		return true;
	})
};

module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		mobileNo: request_body.mobileNo,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((registered_user, error) => {
		if(error){
			return {
				message: error.message
			};
		}

		return {
			message: 'Successfully registered a user!'
		};
	}).catch(error => console.log(error));
};

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		// User does not exist.
		console.log(result);
		if(result == null) {
			return res.send(false)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {
				return res.send({accessToken: auth.createAccessToken(result)})
			} else {
				// If password is incorrect.
				return res.send(false);
			}
		}
	}).catch(err => res.send(err));

};

module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "";

		return res.send(result);
	})
	.catch(err => res.send(err))
};

module.exports.order = async (request, response) => {
	console.log(request.user.id)

	console.log(request.body.productId);

	if (request.user.isAdmin) {
		return response.send({message: "Action Forbidden"})
	}

	let isUserUpdated = await User.findById(request.user.id).then(user => {
		let newOrder = {
			productId : request.body.productId
		}

		user.orders.push(newOrder);

		return user.save().then(user => true).catch(err => err.message)
	})

	if (isUserUpdated !== true) {
		return response.send({message: isUserUpdated})
	};

	let isProductUpdated = await Product.findById(request.body.productId).then(product => {
		let buyer = {
			userId: request.user.id
		}

		product.buyers.push(buyer);

		return product.save().then(product => true).catch(err => err.message)
	}) 

	if (isProductUpdated !== true) {
		return response.send({message: isProductUpdated})
	}

	if (isUserUpdated && isProductUpdated) {
		return response.send(true)
	}
};

module.exports.getOrders = (request, response) => {
	User.findById(request.user.id)
	.then(result => response.send(result.orders))
	.catch(err => response.send(err))
}

module.exports.resetPassword = async (request, response) => {
	try {
		const { newPassword } = request.body;
		const { id } = request.user;

		const hashedPassword = await bcrypt.hash(newPassword , 10);

		await User.findByIdAndUpdate(id, { password: hashedPassword });

		response.status(200).json(true);
	} catch (error) {
		console.log(error);
		response.status(500).json({ message: 'Internal server error' });
	}
};

module.exports.updateProfile = async (request, response) => {
	try {
		const userId = request.user.id;

		const { firstName, lastName, mobileNo } = request.body;

		const updatedUser = await User.findByIdAndUpdate(
			userId,
			{ firstName, lastName, mobileNo },
			{ new: true }
		);

		response.json(updatedUser)
	} catch (error) {
		console.log(error)
		response.status(500).json({ message: 'Failed to update profile' });
	}
}