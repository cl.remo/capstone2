const Product = require('../models/Products.js');
const auth = require('../auth.js');

// register product
module.exports.addProduct = (request, response) => {
	let new_product = new Product({
		name : request.body.name,
		description : request.body.description,
		price : request.body.price
	});

	return new_product.save().then((product, error) => {
		if (error) {
			return response.send(false);
		} else {
			return response.send(true);
		}
	}).catch(error => console.log(error));
};

// lists all products
module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
};

// lists all active products for purchase
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
};

// list specific product
module.exports.getProduct = (request, response) => {
	return Product.findById(request.params.productId).then(result => {
		console.log(result)
		return response.send(result)
	})
	.catch(err => response.send(err))
};

// update product details
module.exports.updateProduct = (request, response) => {
	let updated_product_details = {
		name : request.body.name,
		description : request.body.description,
		price : request.body.price
	};

	return Product.findByIdAndUpdate(request.params.id, updated_product_details).then((product, error) => {

		if (error) {
			return response.send(false);
		} else {
			return response.send(true);
		}
	})
	.catch(err => response.send(err))
};

// archive product
module.exports.archiveProduct = (request, response) => {
	let archive_product = {
		isActive: false
	}

	return Product.findByIdAndUpdate(request.params.id, archive_product).then((product, error) => {

		if (error) {
			return response.send(false)
		} else {
			return response.send(true)
		}
	})
	.catch(err => response.send(err))
};

// activates archived product
module.exports.activateProduct = (request, response) => {
	let activate_product = {
		isActive: true
	}

	return Product.findByIdAndUpdate(request.params.id, activate_product).then((product, error) => {

		if (error) {
			return response.send(false)
		} else {
			return response.send(true)
		}
	})
	.catch(err => response.send(err))
};

module.exports.searchProduct = async (request, response) => {
      try {
      	const { productName } = request.body;

      	const products = await Product.find({ 
      		name: { $regex: productName, $options: 'i' } 
      	});

      	response.json(products);
      } catch (error) {
      	console.error(error);
      	response.status(500).json({ error : 'Internal Server Error'});
      }
};