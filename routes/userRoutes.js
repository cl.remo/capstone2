const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');

const { verify, verifyAdmin } = auth;

// Check if email exists
router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Register user
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Login user
router.post("/login", UserController.loginUser);

// Get user details
router.get("/details", verify, UserController.getProfile)

// get user order
router.post("/order", verify, UserController.order)

// get user's order
router.get("/getOrders", verify, UserController.getOrders)

// reset user's password
router.put("/reset-password", verify, UserController.resetPassword)

// updates user deatils in profile
router.put('/profile', verify, UserController.updateProfile);

module.exports = router;