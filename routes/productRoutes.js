const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;

// add single product
router.post('/', verify, verifyAdmin, ProductController.addProduct);

// get all product
router.get('/all', ProductController.getAllProducts);

// get all active products
router.get('/', ProductController.getAllActiveProducts);

// get single product
router.get ('/:productId', ProductController.getProduct);

// update product details
router.put('/:id', auth.verify, auth.verifyAdmin, ProductController.updateProduct);

// archive product
router.put('/:id/archive', auth.verify, auth.verifyAdmin, ProductController.archiveProduct);

// activate archived product
router.put('/:id/activate', auth.verify, auth.verifyAdmin, ProductController.activateProduct);

// Search product by name
router.post('/search', ProductController.searchProduct);

module.exports = router;